
package examen01poo;

/**
 *
 * @author REDES
 */
public class Examen01poo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //generar objeto construido por omision
         System.out.println("EJEMPLO 1");
        Recibo recibo=new Recibo();
        recibo.setNumRecibo(102);
        recibo.setFecha("21 MARZO 2019");
        recibo.setNombre("Jose Lopez");
        recibo.setDomicilio("Av del Sol 1200");
        recibo.setTipoServicio(1);
        recibo.setCostoPorKilowatts(2.00f);
        recibo.setKilowattsConsumidos(450);
        
        System.out.println("SUBTOTAL: "+recibo.calcularSubtotal());
        System.out.println("IMPUESTO: "+recibo.calcularImpuesto());
        System.out.println("TOTAL A PAGAR: "+recibo.calcularTotal());
        
         System.out.println("");
         System.out.println("EJEMPLO 2");
        Recibo recibo2=new Recibo();
        recibo2.setNumRecibo(103);
        recibo2.setFecha("21 MARZO 2019");
        recibo2.setNombre("Abarrotes feliz");
        recibo2.setDomicilio("Av del Sol");
        recibo2.setTipoServicio(2);
        recibo2.setCostoPorKilowatts(3.00f);
        recibo2.setKilowattsConsumidos(1200);
        
        System.out.println("SUBTOTAL: "+recibo2.calcularSubtotal());
        System.out.println("IMPUESTO: "+recibo2.calcularImpuesto());
        System.out.println("TOTAL A PAGAR: "+recibo2.calcularTotal());
        
        
    }
}
