
package examen01poo;

/**
 *
 * @author REDES
 */
public class Recibo {
    //atributos de la clase
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float costoPorKilowatts;
    private float kilowattsConsumidos;
    //constructores
    public Recibo(){
        this.numRecibo=0;
        this.fecha="";
        this.nombre="";
        this.domicilio="";
        this.tipoServicio=0;
        this.costoPorKilowatts=0;
        this.kilowattsConsumidos=0;
    }

    public Recibo(int numRecibo, String fecha, String nombre, String domicilio, int tipoServicio, float costoPorKilowatts, float kilowattsConsumidos) {
        this.numRecibo = numRecibo;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.costoPorKilowatts = costoPorKilowatts;
        this.kilowattsConsumidos = kilowattsConsumidos;
    }
    
    public Recibo(Recibo otro) {
        this.numRecibo = otro.numRecibo;
        this.fecha = otro.fecha;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.tipoServicio = otro.tipoServicio;
        this.costoPorKilowatts = otro.costoPorKilowatts;
        this.kilowattsConsumidos = otro.kilowattsConsumidos;
    }

    //metodos
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public double getCostoPorKilowatts() {
        return costoPorKilowatts;
    }

    public void setCostoPorKilowatts(float costoPorKilowatts) {
        this.costoPorKilowatts = costoPorKilowatts;
    }

    public double getKilowattsConsumidos() {
        return kilowattsConsumidos;
    }

    public void setKilowattsConsumidos(float kilowattsConsumidos) {
        this.kilowattsConsumidos = kilowattsConsumidos;
    }
    
    //metodos de comportamiento
    public float calcularSubtotal(){
        float subtotal=0.0f;
        switch(this.tipoServicio){
            case 1:
                this.costoPorKilowatts=2.00f;
                subtotal=this.kilowattsConsumidos*this.costoPorKilowatts;
                break;
            case 2:
                this.costoPorKilowatts=3.00f;
                subtotal=this.kilowattsConsumidos*this.costoPorKilowatts;
                break;
            case 3:
                this.costoPorKilowatts=5.00f;
                subtotal=this.kilowattsConsumidos*this.costoPorKilowatts;
                break;
            default:
                System.out.println("Opcion no valida");
        }
        return subtotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubtotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()+this.calcularImpuesto();
    return total;
    }
    
}
